*** Settings ***
Resource                ${EXECDIR}${/}Config${/}Patterns.robot
Resource                ${EXECDIR}${/}Config${/}RapidPassPH-API-Variables.robot
Variables               ${EXECDIR}${/}Config${/}RapidPassPH-Rules.py
Variables               ${EXECDIR}${/}Config${/}RapidPassPH-Spiels.py
Resource                ${EXECDIR}${/}Config${/}RapidPassPH-Web-Variables.robot
Resource                ${EXECDIR}${/}Config${/}Requests-Config.robot
Resource                ${EXECDIR}${/}Config${/}Selenium-Config.robot
Variables               ${EXECDIR}${/}Input${/}RapidPassPH-Default-Values.py
Variables               ${EXECDIR}${/}Input${/}RapidPassPH-Selection-Values.py
Variables               ${EXECDIR}${/}Input${/}Registration.py
Resource                ${EXECDIR}${/}Resources${/}Keywords${/}RapidPassPH-API-Requests.robot
Resource                ${EXECDIR}${/}Resources${/}Keywords${/}RapidPassPH-Helpers.robot
Variables               ${EXECDIR}${/}Resources${/}Resource-Mapping${/}RapidPassPH.py
Resource                ${EXECDIR}${/}Resources${/}Common.robot
Resource                ${EXECDIR}${/}Resources${/}Resource-Mapping${/}PageObjects${/}HomePage.robot
Resource                ${EXECDIR}${/}Resources${/}Resource-Mapping${/}PageObjects${/}OnboardingPage.robot
Resource                ${EXECDIR}${/}Resources${/}Resource-Mapping${/}PageObjects${/}RegistrationPage.robot
Library                 Collections
Library                 DateTime
Library                 FakerLibrary
Library                 OperatingSystem
Library                 RequestsLibrary
Library                 SeleniumLibrary
Library                 String

*** Variables ***
${IMAGE_IMAGE1}         ${EXECDIR}${/}Input${/}chair.png

*** Keywords ***
Begin Test
    [Documentation]         Conducts a health check on the System Under Test base URL. Upon success, opens a browser then goes to the said URL.
    [Arguments]             ${sut_url}  ${current_os}=${NONE}
    ${sys_platform}=    Evaluate    sys.platform    sys
    ${current_os}=  Set Variable If     '${current_os}' == '${NONE}'    ${sys_platform}
    ${current_os}=  Convert To Lowercase    ${current_os}
    Common.Critical Health Check    ${sut_url}
    Common.Setup and Open Web Browser   ${sut_url}  current_os=${current_os}
    Common.Wait Until Generic Page Finishes Loading

Critical Health Check
    [Documentation]         Conducts a health check on the given URL via ``HTTP GET`` request method.
    ...
    ...                     Raises a _Fatal Error_ on failure.
    [Arguments]             ${URL}
    ${is_healthy}=          Common.Non-critical Health Check   ${URL}
    Run Keyword Unless      ${is_healthy}   Fatal Error     msg=Cannot connect to given URL

Does Require Certificate
    [Documentation]         Returns ``True`` if the given protocol is equal to ``https``; otherwise, returns ``False``;
    [Arguments]             ${protocol}
    Should Be Equal As Strings      ${protocol}     https

End Test
    [Documentation]         Closes the browser and destorys all created request sessions.
    Close Browser
    Delete All Sessions

Generate UUIDv4 String
    [Documentation]         Returns a UUID v4 string
    ${uuid_v4}=             Evaluate    str(__import__('uuid').uuid4())
    [Return]                ${uuid_v4}

Get Current Epoch Timestamp With Milliseconds
    [Documentation]         Returns the current timestamp in epoch format.
    ${current_date}=        Get Current Date    result_format=epoch
    ${current_timestamp_with_decimal}=      Evaluate                ${current_date} * 1000
    ${current_timestamp_without_decimal}=   Convert To Integer      ${current_timestamp_with_decimal}
    [Return]                ${current_timestamp_without_decimal}

Get Element Bounding Client Rectangle
    [Documentation]         Returns the values returned by [Element.getBoundingClientRect()|https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect] in object format.
    [Arguments]             ${locator}
    ${strategy}             ${selector}=    Common.Get Locator Value From Strategy-Locator Value Dictionary     ${locator}
    ${query}=               Common.Use DOM Element Query    ${strategy}     ${selector}
    ${element_bound_client_rectangle}=  Execute Javascript  return ${query}.getBoundingClientRect()
    [Return]                ${element_bound_client_rectangle}

Get Img Src
    [Documentation]         Returns the source _URL_ of an element.
    [Arguments]             ${locator}
    ${img_url}=             Get Element Attribute   ${locator}@src
    [Return]                ${img_url}

Get Locator Value From Strategy-Locator Value Dictionary
    [Documentation]         Returns the Locator Value from the Strategy-Locator Value.
    [Arguments]             ${strategy_locator_value_dictionary}
    ${strategy}             ${locator_value}=   Split String    ${strategy_locator_value_dictionary}    :       max_split=1
    [Return]                ${strategy}     ${locator_value}

Get Protocol
    [Documentation]         Returns the first five characters of the given _URL_.
    [Arguments]             ${URL}
    ${protocol}=            Get Substring   string=${URL}      start=0       end=5
    [Return]                ${protocol}

Get Screen Width And Height
    [Documentation]         Returns the screen's width and height in pixels as tuple
    ${width}=               Execute Javascript      return window.screen.width
    ${height}=              Execute Javascript      return window.screen.height
    [Return]                ${width}    ${height}

Non-critical Health Check
    [Documentation]         Returns ``True`` if the given _URL_ responded with a status code ``200``; otherwise, returns ``False``.
    ...
    ...                     Conducts a health check on the given URL via ``HTTP GET`` request method.
    ...
    ...                     Does *not* raise a _Fatal Error_ on failure.
    [Arguments]             ${URL}
    [Teardown]              Delete All Sessions
    ${protocol}=            Common.Get Protocol     ${URL}
    ${session_verify}=      Run Keyword And Return Status           Common.Does Require Certificate     ${protocol}
    Create Session          health_check            ${URL}          verify=${session_verify}
    ${resp_health_check}=   Run Keyword And Continue On Failure     Get Request             health_check    /   allow_redirects=True        timeout=${VISIT_URL_TIMEOUT}
    Run Keyword And Ignore Error    Log     ${resp_health_check.history}
    Run Keyword And Ignore Error    Log     ${resp_health_check.url}
    Run Keyword And Ignore Error    Log     ${resp_health_check.headers}
    Run Keyword And Ignore Error    Log     ${resp_health_check.text}
    ${is_healthy}=          Run Keyword And Return Status           Should Be Equal As Strings          ${resp_health_check.status_code}     200
    [Return]                ${is_healthy}

Pick Random Index From Arguments
    [Documentation]         Returns a random index from the given list
    [Arguments]             @{list}
    ${list_length}=         Get Length  ${list}
    ${random_index}=        Evaluate    random.randint(0, ${list_length}-1)    modules=random
    [Return]                ${random_index}

Pick Randomly From Arguments
    [Documentation]         Returns a random object from the given list.
    [Arguments]             @{list}
    ${random_index}=        Common.Pick Random Index From Arguments     @{list}
    [Return]                ${list[${random_index}]}

Pick Randomly From Integer Range
    [Documentation]         Returns a random integer from the given range.
    ...
    ...                     Range covers both min and max. [_min_, _max_]
    [Arguments]             ${min}      ${max}
    ${integer}=             Evaluate    random.randint(${min}, ${max})    modules=random
    [Return]                ${integer}

Get Value From Pair
    [Documentation]     Returns the value of a key-value pair when the key is unknown
    [Arguments]         ${pair}
    ${key}=             Get Dictionary Keys     ${pair}
    ${value}=           Get From Dictionary     ${pair}     ${key}[0]
    [Return]            ${value}

Scroll Page To Bottom
    [Documentation]         Scrolls the page to bottom-most section of the screen.
    Common.Scroll Page To Location  0   Number.MAX_SAFE_INTEGER     #javascript number attribute; Number.MAX_SAFE_INTEGER = 9007199254740991

Scroll Page To Location
    [Documentation]         Scrolls the page to _x_ and _y_.
    [Arguments]             ${x}    ${y}
    Execute JavaScript      window.scrollTo(${x},${y})

Scroll Page To Top
    [Documentation]         Scrolls the page to top-most section of the screen.
    Common.Scroll Page To Location  0   Number.MIN_SAFE_INTEGER     #javascript number attribute; Number.MIN_SAFE_INTEGER = -9007199254740991

Scroll To Element
    [Documentation]         Sets the screen focus to specific element.
    [Arguments]             ${locator}
    ${strategy}     ${selector}=    Common.Get Locator Value From Strategy-Locator Value Dictionary     ${locator}
    ${query}=       Use DOM Element Query   ${strategy}     ${selector}
    Execute Javascript  ${query}.scrollIntoView(true)

Setup And Open Web Browser
    [Documentation]         Initiates the browser and launches it.
    [Arguments]             ${sut_url}      ${alias}=${SUT_BROWSER_ALIAS}   ${current_os}=darwin
    ${current_os}=  Convert To Lowercase    ${current_os}
    ${options}=             Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver
    ${options.set_binary}=  Set Variable If     '${current_os}' == 'darwin'     set_binary=/Applications/Google Chrome.app/Contents/MacOS/Google Chrome
    Call Method     ${options}      add_argument    headless
    # End setup
    Run Keyword If  '${BROWSER}'=='headless chrome'
    ...     Run Keywords    Create WebDriver    Chrome      alias=${alias}      chrome_options=${options}
    ...         AND         Go To   ${sut_url}
    ...     ELSE    Open Browser    ${sut_url}      ${BROWSER}      ${alias}
    Maximize Browser Window
    ${width}    ${height}=      Common.Get Screen Width and Height
    Set Window Size     ${width}    ${height}
    Set Window position     0   0

Use DOM Element Query
    [Documentation]         Returns the JQuery statement based on the ``strategy`` parameter
    [Arguments]             ${strategy}         ${selector}
    ${query}=               Set Variable If     "${strategy}"=="id"     document.getElementById('${selector}')
    ...                                     "${strategy}"=="name"   document.getElementsByName('${selector}')[0]    #returns the first DOM element
    ...                                     "${strategy}"=="css"    document.querySelector('${selector}')
    ...                                     "${strategy}"=="xpath"  document.evaluate("${selector}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue
    [Return]                ${query}

Wait Until Generic Page Finishes Loading
    [Documentation]         Fails if the page did not finish loading after ${PAGE_LOAD_TIMEOUT}.
    Wait For Condition      return document.readyState === "complete"       timeout=${PAGE_LOAD_TIMEOUT}
