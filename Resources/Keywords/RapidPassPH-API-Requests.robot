*** Settings ***
Documentation               Manages RapidPassPH Requests
Resource                    ${EXECDIR}${/}Resources${/}Common.robot

*** Keywords ***
getAccessPasses
    [Documentation]         Returns the response object of getAccessPasses
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    ${response_object}=     Get Request         rapidpassph      /registry/access-passes
    Log                     ${response_object.content}
    [Return]                ${response_object}

getAccessPassDetails
    [Documentation]         Returns the response object of getAccessPassDetails
    [Arguments]             ${referenceId}
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    ${response_object}=     Get Request         rapidpassph      /registry/access-passes/${referenceId}
    Log                     ${response_object.content}
    [Return]                ${response_object}

Single newRequestPass
    [Documentation]         Returns the response object of Single newRequestPass and its submitted data
    [Arguments]             ${aporType}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['APORTYPE']}
    ...                     ${company}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['COMPANY']}
    ...                     ${destCity}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['DESTCITY']}
    ...                     ${destName}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['DESTNAME']}
    ...                     ${destProvince}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['DESTPROVINCE']}
    ...                     ${destStreet}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['DESTSTREET']}
    ...                     ${email}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['EMAIL']}
    ...                     ${firstName}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['FIRSTNAME']}
    ...                     ${idType}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['IDTYPE']}
    ...                     ${identifierNumber}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['IDENTIFIERNUMBER']}
    ...                     ${lastName}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['LASTNAME']}
    ...                     ${middleName}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['MIDDLENAME']}
    ...                     ${mobileNumber}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['MOBILENUMBER']}
    ...                     ${originCity}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['ORIGINCITY']}
    ...                     ${originName}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['ORIGINNAME']}
    ...                     ${originProvince}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['ORIGINPROVINCE']}
    ...                     ${originStreet}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['ORIGINSTREET']}
    ...                     ${passType}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['PASSTYPE']}
    ...                     ${plateNumber}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['PLATENUMBER']}
    ...                     ${remarks}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['REMARKS']}
    ...                     ${suffix}=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['SUFFIX']}
    # Appending default values with epoch_timestamp to generate unique data
    ${epoch_timestamp}=     Common.Get Current Epoch Timestamp With Milliseconds
    ${company}=             Set Variable If     '${company}'=='${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['COMPANY']}'                  ${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['COMPANY']}${epoch_timestamp}            ${company}
    ${firstName}=           Set Variable If     '${firstName}'=='${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['FIRSTNAME']}'              ${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['FIRSTNAME']}${epoch_timestamp}          ${firstName}
    ${lastName}=            Set Variable If     '${lastName}'=='${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['LASTNAME']}'                ${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['LASTNAME']}${epoch_timestamp}           ${lastName}
    ${middleName}=          Set Variable If     '${middleName}'=='${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['MIDDLENAME']}'            ${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL['MIDDLENAME']}${epoch_timestamp}         ${middleName}
    # Constructing request
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    &{request_headers}=     Create Dictionary   Content-Type=application/json
    &{request_data}=        Create Dictionary   aporType=${aporType}
    ...                                         company=${company}
    ...                                         destCity=${destCity}
    ...                                         destName=${destName}
    ...                                         destProvince=${destProvince}
    ...                                         destStreet=${destStreet}
    ...                                         email=${email}
    ...                                         firstName=${firstName}
    ...                                         idType=${idType}
    ...                                         identifierNumber=${identifierNumber}
    ...                                         lastName=${lastName}
    ...                                         middleName=${middleName}
    ...                                         mobileNumber=${mobileNumber}
    ...                                         originCity=${originCity}
    ...                                         originName=${originName}
    ...                                         originProvince=${originProvince}
    ...                                         originStreet=${originStreet}
    ...                                         passType=${passType}
    ...                                         plateNumber=${plateNumber}
    ...                                         remarks=${remarks}
    ...                                         suffix=${suffix}
    # Sending request and receiving response
    ${response_object}=     Post Request        rapidpassph      /registry/access-passes
    ...                                         headers=${request_headers}
    ...                                         data=${request_data}
    Log                     ${response_object.content}
    [Return]                ${response_object}  ${request_data}

revokeAccessPass
    [Documentation]         Returns the response object of getAccessPassDetails
    [Arguments]             ${referenceId}
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    ${response_object}=     Delete Request         rapidpassph      /registry/access-passes/${referenceId}
    Log                     ${response_object.content}
    [Return]                ${response_object}

updateAccessPass
    [Documentation]         Returns the response object of updateAccessPass and its submitted data
    [Arguments]             ${referenceId}
    # ...                     ${aporType}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['APORTYPE']}
    # ...                     ${company}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['COMPANY']}
    # ...                     ${controlCode}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['CONTROLCODE']}
    # ...                     ${destCity}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['DESTCITY']}
    # ...                     ${destName}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['DESTNAME']}
    # ...                     ${destProvince}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['DESTPROVINCE']}
    # ...                     ${destStreet}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['DESTSTREET']}
    # ...                     ${idType}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['IDTYPE']}
    # ...                     ${identifierNumber}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['IDENTIFIERNUMBER']}
    # ...                     ${name}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['NAME']}
    # ...                     ${passType}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['PASSTYPE']}
    # ...                     ${remarks}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['REMARKS']}
    ...                     ${status}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['STATUS']}
    # ...                     ${validFrom}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['VALIDFROM']}
    # ...                     ${validUntil}=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['VALIDUNTIL']}
    # # Appending default values with epoch_timestamp to generate unique data
    # ${epoch_timestamp}=     Common.Get Current Epoch Timestamp With Milliseconds
    # ${company}=             Set Variable If     '${company}'=='${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['COMPANY']}'                   ${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['COMPANY']}${epoch_timestamp}                 ${company}
    # ${name}=                Set Variable If     '${name}'=='${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['NAME']}'                         ${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['NAME']}${epoch_timestamp}                    ${name}
    # # Replacing default value of controlCode with UUID v4 string if controlCode is using default value
    # ${uuid_v4}=             Common.Generate UUIDv4 String
    # ${controlCode}=         Set Variable If     '${controlCode}'=='${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['CONTROLCODE']}'           ${uuid_v4}                                                                                  ${controlCode}
    # # Replacing default value of validFrom with valid validFrom custom DateTime format if validFrom is using default value
    # ${rapidpassph_datetime_now}=        RapidPassPH-Helpers.Get Current Timestamp In RapidPassPH Custom DateTime Format
    # ${validFrom}=           Set Variable If     '${validFrom}'=='${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['VALIDFROM']}'               ${rapidpassph_datetime_now}                                                                 ${validFrom}
    # # Replacing default value of validUntil with valid validUntil custom DateTime format if validUntil is using default value
    # ${agreed_valid_until_value}=        RapidPassPH-Helpers.Convert DateTime To RapidPassPH Custom DateTime Format                          ${year}=${RAPIDPASSPH_DEFAULT_VALUES_VALID_UNTIL['YEAR']}
    # ...                                                                                                                                     ${month}=${RAPIDPASSPH_DEFAULT_VALUES_VALID_UNTIL['MONTH']}
    # ...                                                                                                                                     ${day}=${RAPIDPASSPH_DEFAULT_VALUES_VALID_UNTIL['DAY']}
    # ${validUntil}=          Set Variable If     '${validUntil}'=='${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['VALIDUNTIL']}'             ${agreed_valid_until_value}                                                                 ${validUntil}
    # Constructing request
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    &{request_headers}=     Create Dictionary   Content-Type=application/json
    &{request_data}=        Create Dictionary   #aporType=${aporType}
    # ...                                         company=${company}
    # ...                                         controlCode=${controlCode}
    # ...                                         destCity=${destCity}
    # ...                                         destName=${destName}
    # ...                                         destProvince=${destProvince}
    # ...                                         destStreet=${destStreet}
    # ...                                         idType=${idType}
    # ...                                         identifierNumber=${identifierNumber}
    # ...                                         name=${name}
    # ...                                         passType=${passType}
    # ...                                         referenceId=${referenceId}
    # ...                                         remarks=${remarks}
    ...                                         status=${status}
    # ...                                         validFrom=${validFrom}
    # ...                                         validUntil=${validUntil}
    # Sending request and receiving response
    ${response_object}=     Put Request         rapidpassph      /registry/access-passes/${referenceId}
    ...                                         headers=${request_headers}
    ...                                         data=${request_data}
    Log                     ${response_object.content}
    [Return]                ${response_object}  ${request_data}
