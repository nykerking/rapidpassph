*** Settings ***
Documentation               Manages RapidPassPH Requests
Resource                    ${EXECDIR}${/}Resources${/}Common.robot

*** Keywords ***
Convert DateTime To RapidPassPH Custom DateTime Format
    [Documentation]         Converts given year, month, day, hour, minute, second, and millisecond to RapidPassPH custom DateTime format.
    [Arguments]             ${year}=0
    ...                     ${month}=0
    ...                     ${day}=0
    ...                     ${hour}=0
    ...                     ${minute}=0
    ...                     ${second}=0
    ...                     ${millisecond}=0
    ${rapidpassph_datetime}=        Evaluate        __import__('pendulum').datetime(${year}, ${month}, ${day}, ${hour}, ${minute}, ${second}, ${millisecond}).in_tz('UTC').format('${RAPIDPASSPH_PENDULUM_CUSTOM_DATETIME_FORMAT}')

Create INDIVIDUAL passType RapidPass Using mobileNumber: ${mobileNumber}
    [Documentation]         Returns the response object of Single newRequestPass and its submitted data.
    ...                     Fails if creation is unsuccessful.
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass              mobileNumber=${mobileNumber}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      201         ${response_object_single_newrequestpass.status_code}
    [Return]                ${response_object_single_newrequestpass}        ${request_data_single_newrequestpass}

Ensure RapidPass Can Be Recreated
    [Documentation]         Returns boolean ${True} if given RapidPass is in DECLINED or SUSPENDED status. Else, returns boolean ${False}.
    ...                     Fails if given RapidPass is not DECLINED or SUSPENDED.
    [Arguments]             ${referenceId}
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails       ${referenceId}
    ${is_recreatable}=      Run Keyword AND Return Status       List Should Contain Value               ${RAPIDPASSPH_API_RULES['RECREATABLE_RAPIDPASS_STATUS']}        ${response_object_getaccesspassdetails.json()['status']}
    Run Keyword And Continue On Failure             List Should Contain Value               ${RAPIDPASSPH_API_RULES['RECREATABLE_RAPIDPASS_STATUS']}        ${response_object_getaccesspassdetails.json()['status']}
    [Return]                ${is_recreatable}

Extract referenceId from Single newRequestPass
    [Documentation]         Returns the referenceId of given single newRequestPass response object
    [Arguments]             ${response_object_single_newrequestpass}
    [Return]                ${response_object_single_newrequestpass.json()['referenceId']}

Find A RapidPass By Given Key
    [Documentation]         Returns the match as array.
    [Arguments]             ${getAccessPasses_response_object}    ${key_to_search}
    ${query}=               Evaluate    __import__('jsonpath_rw_ext').match("$[?(@.name=='${key_to_search}')]", ${getAccessPasses_response_object.json()})
    [Return]                ${query}

Generate Random Single newRequestPass Details
    [Documentation]         Return a dictionary containing random details for a requester_type to be used for newAccessPass requests.
    ...                     Defaults to individual. Valid values for requester_type: individual, vehicle
    [Arguments]             ${requester_type}=individual
    ${requester_type}=      Convert To Lowercase            ${requester_type}
    ${aporType}=            Common.Pick Randomly From Arguments         @{RAPIDPASSPH_SELECTION_VALUES_APORTYPE}
    ${company}=             FakerLibrary.Company
    ${destCity}=            FakerLibrary.City
    ${destName}=            FakerLibrary.Company
    ${destProvince}=        FakerLibrary.State
    ${destStreet}=          FakerLibrary.Street Address
    ${email}=               FakerLibrary.Email
    ${firstName}=           FakerLibrary.First Name
    ${idType}=              Set Variable If                             '${requester_type}'=='individual'           PersonalID
    ...                                                                 '${requester_type}'=='vehicle'              VehicleID
    ${random_plate_digits}=     FakerLibrary.Random Number      3       ${TRUE}
    ${random_plate_letters}=    Generate Random String          length=3            chars=[UPPER]
    ${identifierNumber}=    Catenate                        SEPARATOR=      ${random_plate_letters}         ${random_plate_digits}
    ${lastName}=            FakerLibrary.Last Name
    ${middleName}=          FakerLibrary.First Name Female
    ${random_digits}=       FakerLibrary.Random Number      9           ${TRUE}
    ${mobileNumber}=        Catenate                        SEPARATOR=      09      ${random_digits}
    ${originCity}=          FakerLibrary.City
    ${originName}=          FakerLibrary.Company
    ${originProvince}=      FakerLibrary.State
    ${originStreet}=        FakerLibrary.Street Address
    ${passType}=            Set Variable If                             '${requester_type}'=='individual'           INDIVIDUAL
    ...                                                                 '${requester_type}'=='vehicle'              VEHICLE
    ${plateNumber}=         Set Variable If                             '${requester_type}'=='individual'           ${EMPTY}
    ...                                                                 '${requester_type}'=='vehicle'              ${identifierNumber}
    ${remarks}=             FakerLibrary.Paragraph                      nb_sentences=1                              variable_nb_sentences=False
    ${suffix}=              FakerLibrary.Suffix
    &{single_newRequestPass_details}=       Create Dictionary           aporType=${aporType}
    ...                                                                 company=${company}
    ...                                                                 destCity=${destCity}
    ...                                                                 destName=${destName}
    ...                                                                 destProvince=${destProvince}
    ...                                                                 destStreet=${destStreet}
    ...                                                                 email=${email}
    ...                                                                 firstName=${firstName}
    ...                                                                 idType=${idType}
    ...                                                                 identifierNumber=${identifierNumber}
    ...                                                                 lastName=${lastName}
    ...                                                                 middleName=${middleName}
    ...                                                                 mobileNumber=${mobileNumber}
    ...                                                                 originCity=${originCity}
    ...                                                                 originName=${originName}
    ...                                                                 originProvince=${originProvince}
    ...                                                                 originStreet=${originStreet}
    ...                                                                 passType=${passType}
    ...                                                                 plateNumber=${plateNumber}
    ...                                                                 remarks=${remarks}
    ...                                                                 suffix=${suffix}
    Log Dictionary          ${single_newRequestPass_details}
    [Return]                ${single_newRequestPass_details}

Get Current Timestamp In RapidPassPH Custom DateTime Format
    [Documentation]         Returns the current timestamp in RapidPassPH custom DateTime format.
    ${rapidpassph_datetime_now}=    Evaluate                __import__('pendulum').now('UTC').format('${RAPIDPASSPH_PENDULUM_CUSTOM_DATETIME_FORMAT}')
    [Return]                ${rapidpassph_datetime_now}

Revoke RapidPass With referenceId: ${referenceId}
    [Documentation]         Sets the status of a given ${referenceId} to SUSPENDED status.
    ${response_object_revokeaccesspass}=        RapidPassPH-API-Requests.revokeAccessPass           ${referenceId}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      200         ${response_object_revokeaccesspass.status_code}
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails       ${referenceId}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_getaccesspassdetails.json()}      status                  SUSPENDED

Set RapidPass With referenceId: ${referenceId} To Valid Status: ${valid_status}
    [Documentation]         Sets the status of a given ${referenceId} to ${valid_status}.
    ...                     Returns the response object of updateAccessPass and its submitted data.
    ...                     Fails if update is unsuccessful.
    ${response_object_updateaccesspass}         ${request_data_updateaccesspass}=           RapidPassPH-API-Requests.updateAccessPass           ${referenceId}          status=${valid_status}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      200         ${response_object_updateaccesspass.status_code}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_updateaccesspass.json()}          status                  ${valid_status}
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails       ${referenceId}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_getaccesspassdetails.json()}      status                  ${valid_status}
    [Return]                ${response_object_updateaccesspass}             ${request_data_updateaccesspass}
