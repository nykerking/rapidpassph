RAPIDPASSPH_HOME_PAGE = {
	"BUTTON_APPLY_FOR_A_RAPIDPASS": "xpath://button[normalize-space(descendant-or-self::text())='Apply for a RapidPass']",
	"BUTTON_CHECK_APPLICATION_STATUS": "xpath://button[normalize-space(descendant-or-self::text())='Check Application Status']",
	"FOOTER": {
		"LINK_ABOUT": "xpath://footer/a[normalize-space(descendant-or-self::text())='About']",
		"LINK_FAQS": "xpath://footer/a[normalize-space(descendant-or-self::text())='FAQs']",
		"LINK_CONTACT_US": "xpath://footer/a[normalize-space(descendant-or-self::text())='Contact Us']",
		"LINK_PRIVACY_POLICY": "xpath://footer/a[normalize-space(descendant-or-self::text())='Privacy Policy']"
	}
}
