*** Variables ***
# Selectors
${INPUT_FIRST_NAME}=            css:[formcontrolname='firstName']
${INPUT_MIDDLE_NAME}=           css:[formcontrolname='middleName']
${INPUT_LAST_NAME}=             css:[formcontrolname='lastName']
${INPUT_SUFFIX}=                css:[formcontrolname='suffix']
${INPUT_EMAIL}=                 css:[formcontrolname='email']
${INPUT_MOBILE_NUMBER}=         css:[formcontrolname='mobileNumber']
${INPUT_COMPANY}=               css:[formcontrolname='company']
${INPUT_ID_NUMBER}=             css:[formcontrolname='identifierNumber']
${INPUT_ORIGIN_STREET}=         css:[formcontrolname='originStreet']
${INPUT_ORIGIN_CITY}=           css:[formcontrolname='originCity']
${INPUT_ORIGIN_PROVINCE}=       css:[formcontrolname='originProvince']
${INPUT_DESTINATION_STREET}=    css:[formcontrolname='destStreet']
${INPUT_DESTINATION_CITY}=      css:[formcontrolname='destCity']
${INPUT_DESTINATION_PROVINCE}=  css:[formcontrolname='destProvince']
${INPUT_REASON}=                css:[formcontrolname='remarks']
${INPUT_VEHICLE_ID}=            css:[formcontrolname='plateNumber']
${SELECT_ID_TYPE}=              xpath://input[@formcontrolname="idType"]/ancestor::div[contains(@class, "mat-form-field-flex")]
${SELECT_NATURE_OF_WORK}=       xpath://input[@formcontrolname="aporType"]/ancestor::div[contains(@class, "mat-form-field-flex")]
${SELECT_VEHICLE_ID_TYPE}=      xpath://label[contains(text(), "Vehicle ID Type")]/following-sibling::mat-form-field//input
${CHECKBOX_TOS}=                css:.mat-checkbox-inner-container
${BTN_SUBMIT}=                  css:button[type='submit']
${SELECT_OPTION}=               xpath://div[contains(text(), "$")]/ancestor::div[@class='ng-star-inserted']
${SELECT_LABEL_ERROR}=          xpath://label[contains(text(), "$")]/following-sibling::mat-form-field/descendant::mat-error
${LABEL_APPLICATION_STATUS}=    css:.status-text > div:nth-child(2)
${LABEL_PAGE_NOTIF}=            css:.mat-simple-snackbar > span

# Spiels/Strings
${SPIEL_SUBMIT_SUCCESS}=        You have successfully
${SPIEL_BLANK_FIELD}=           Required
${SPIEL_GENERIC_ERROR}=         Something went wrong. Please Try Again.
${SPIEL_FIELD_ERROR}=           Some fields are invalid. Please review the form and try again.
${SPIEL_INVALID_FORMAT}=        Invalid $ format
${SPIEL_NUMERIC_ERROR}=         Numbers are not allowed
${SPIEL_SPACE_ERROR}=           Value should not be empty
${SPIEL_INVALID_VALUE}=         Not a valid $
${SPIEL_LENGTH_ERROR}=          Maximum length exceeded
${SPIEL_ID_NUMBER_ERROR}=       Only letters, numbers, and dash(-) are allowed
${NEW_APPLICATION_STATUS}=      Pending
${FORM_PAGE_TITLE}=             RapidPass Application

*** Keywords ***
Perform Application
    [Arguments]                         ${firstName}
    ...                                 ${lastName}
    ...                                 ${email}
    ...                                 ${mobileNumber}
    ...                                 ${company}
    ...                                 ${idNumber}
    ...                                 ${originStreet}
    ...                                 ${originCity}
    ...                                 ${originProvince}
    ...                                 ${destStreet}
    ...                                 ${destCity}
    ...                                 ${destProvince}
    ...                                 ${aporType}
    ...                                 ${tos}
    ...                                 ${applicationType}=individual
    ...                                 ${middleName}=${EMPTY}
    ...                                 ${suffix}=${EMPTY}
    ...                                 ${idType}=${EMPTY}
    ...                                 ${remarks}=${EMPTY}
    ...                                 ${vehicleIdType}=${EMPTY}
    ...                                 ${vehicleId}=${EMPTY}
    Common.Wait Until Generic Page Finishes Loading
    Wait Until Element Is Visible       ${INPUT_FIRST_NAME}
    Wait Until Element Is Visible       ${BTN_SUBMIT}
    Input Text                          ${INPUT_FIRST_NAME}                     ${firstName}
    Input Text                          ${INPUT_MIDDLE_NAME}                    ${middleName}
    Input Text                          ${INPUT_LAST_NAME}                      ${lastName}
    Input Text                          ${INPUT_SUFFIX}                         ${suffix}
    Input Text                          ${INPUT_EMAIL}                          ${email}
    Input Text                          ${INPUT_MOBILE_NUMBER}                  ${mobileNumber}
    Input Text                          ${INPUT_COMPANY}                        ${company}
    Run Keyword If                      "${applicationType}" == "individual" and "${idType}" != "${EMPTY}"   Select Individual ID Type   ${idType}
    Input Text                          ${INPUT_ID_NUMBER}                      ${idNumber}
    Input Text                          ${INPUT_ORIGIN_STREET}                  ${originStreet}
    Input Text                          ${INPUT_ORIGIN_CITY}                    ${originCity}
    Input Text                          ${INPUT_ORIGIN_PROVINCE}                ${originProvince}
    Input Text                          ${INPUT_DESTINATION_STREET}             ${destStreet}
    Input Text                          ${INPUT_DESTINATION_CITY}               ${destCity}
    Input Text                          ${INPUT_DESTINATION_PROVINCE}           ${destProvince}
    Run Keyword Unless                  '${aporType}' == '${EMPTY}'             Select Nature Of Work       ${aporType}
    Run Keyword If                      '${applicationType}' == 'vehicle'       Enter Vehicle Details       ${vehicleIdType}    ${vehicleId}
    Input Text                          ${INPUT_REASON}                         ${remarks}
    Run Keyword If                      ${tos} == ${TRUE}                       Click Element               ${CHECKBOX_TOS}
    Click Element                       ${BTN_SUBMIT}

Wait For Application Result Page
    Wait Until Page Contains            Your RapidPass                          timeout=15

Get Application Status
    ${status}=                          Get Text                                ${LABEL_APPLICATION_STATUS}
    [Return]                            ${status}

Select Individual ID Type
    [Arguments]                         ${idType}
    Click Element                       ${SELECT_ID_TYPE}
    ${OPTION_ID_TYPE}=                  Replace String                          ${SELECT_OPTION}            $                   ${idType}
    Scroll Element Into View            ${OPTION_ID_TYPE}
    Click Element                       ${OPTION_ID_TYPE}

Select Nature Of Work
    [Arguments]                         ${aporType}
    Click Element                       ${SELECT_NATURE_OF_WORK}
    ${OPTION_NATURE_OF_WORK}=           Replace String                          ${SELECT_OPTION}            $                   ${aporType}
    Scroll Element Into View            ${OPTION_NATURE_OF_WORK}
    Click Element                       ${OPTION_NATURE_OF_WORK}

Select Vehicle ID Type
    [Arguments]                         ${vehicleIdType}
    Click Element                       ${SELECT_VEHICLE_ID_TYPE}
    ${OPTION_ID_TYPE}=                  Replace String                          ${SELECT_OPTION}            $                   ${vehicleIdType}
    Click Element                       ${OPTION_ID_TYPE}

Enter Vehicle Details
    [Arguments]                         ${vehicleIdType}                        ${vehicleId}
    Run Keyword Unless                  '${vehicleIdType}' == '${EMPTY}'        Select Vehicle ID Type      ${vehicleIdType}
    Input Text                          ${INPUT_VEHICLE_ID}                     ${vehicleId}

Assert Input Error
    [Arguments]                         ${inputFieldName}
    ...                                 ${fieldErrorLabel}
    ...                                 ${errorSpiel}=${SPIEL_FIELD_ERROR}
    ${LABEL_INPUT_ERROR}=               Replace String                          ${SELECT_LABEL_ERROR}       $                   ${inputFieldName}
    Wait Until Element Is Visible       ${LABEL_INPUT_ERROR}
    Element Text Should Be              ${LABEL_INPUT_ERROR}                    ${fieldErrorLabel}