*** Variables ***
# Selectors
${BTN_APPLY_FOR_RAPID_PASS}=    xpath://button[normalize-space(descendant-or-self::text())='Apply for a RapidPass']

*** Keywords ***
Begin Apply For Pass
    Click Element    ${BTN_APPLY_FOR_RAPID_PASS}