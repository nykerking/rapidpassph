*** Variables ***
${BTN_NEXT}=                    xpath://span[contains(text(), 'Next')]/parent::button
${BTN_REGISTER_VEHICLE}=        xpath://span[contains(text(), 'Register a Vehicle')]/parent::button
${BTN_REGISTER_INDIVIDUAL}=     xpath://span[contains(text(), 'Register an Individual')]/parent::button

*** Keywords ***
Proceed With Onboarding
    [Arguments]                         ${application_type}=individual
    Wait Until Page Contains Element    ${BTN_NEXT}
    Click Element                       ${BTN_NEXT}
    ${application_type}=                Convert To Lowercase                    ${application_type}
    Run Keyword If                      '${application_type}' == 'individual'   Click Element                       ${BTN_REGISTER_INDIVIDUAL}
    ...                                 ELSE IF                                 '${application_type}' == 'vehicle'  Click Element   ${BTN_REGISTER_VEHICLE}