*** Settings ***
Force Tags      References: JIRA-Ticket-ID
Resource        ${EXECDIR}${/}Resources${/}Common.robot

*** Variables ***
${MOBILENUMBER}         ${RAPIDPASSPH_DEFAULT_VALUES_GETACCESSPASSDETAILS['MOBILENUMBER']}

*** Test Cases ***
getAccessPassDetails should return and reflect correct key-value pair from Single newRequestPass when passType is INDIVIDUAL
    [Documentation]         Fails if HTTP status code is not 200.
    ...                     Fails if response body json key-value pair is not found.
    # Setup
    ${single_newRequestPass_details}=           RapidPassPH-Helpers.Generate Random Single newRequestPass Details       requester_type=INDIVIDUAL
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass          aporType=${single_newRequestPass_details}[aporType]
    ...                                                                                                                                             company=${single_newRequestPass_details}[company]
    ...                                                                                                                                             destCity=${single_newRequestPass_details}[destCity]
    ...                                                                                                                                             destName=${single_newRequestPass_details}[destName]
    ...                                                                                                                                             destProvince=${single_newRequestPass_details}[destProvince]
    ...                                                                                                                                             destStreet=${single_newRequestPass_details}[destStreet]
    ...                                                                                                                                             email=${single_newRequestPass_details}[email]
    ...                                                                                                                                             firstName=${single_newRequestPass_details}[firstName]
    ...                                                                                                                                             idType=${single_newRequestPass_details}[idType]
    ...                                                                                                                                             identifierNumber=${single_newRequestPass_details}[identifierNumber]
    ...                                                                                                                                             lastName=${single_newRequestPass_details}[lastName]
    ...                                                                                                                                             middleName=${single_newRequestPass_details}[middleName]
    ...                                                                                                                                             mobileNumber=${MOBILENUMBER}
    ...                                                                                                                                             originCity=${single_newRequestPass_details}[originCity]
    ...                                                                                                                                             originName=${single_newRequestPass_details}[originName]
    ...                                                                                                                                             originProvince=${single_newRequestPass_details}[originProvince]
    ...                                                                                                                                             originStreet=${single_newRequestPass_details}[originStreet]
    ...                                                                                                                                             passType=${single_newRequestPass_details}[passType]
    ...                                                                                                                                             plateNumber=${single_newRequestPass_details}[plateNumber]
    ...                                                                                                                                             remarks=${single_newRequestPass_details}[remarks]
    ...                                                                                                                                             suffix=${single_newRequestPass_details}[suffix]
    # Main Test
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails           referenceId=${MOBILENUMBER}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      200         ${response_object_getaccesspassdetails.status_code}
    ${expected_response_body_getaccesspassdetails}=                     Copy Dictionary     ${request_data_single_newrequestpass}
    Keep In Dictionary      ${expected_response_body_getaccesspassdetails}      @{RAPIDPASSPH_API_RULES['GETACCESSPASSDETAILS']['EXPECTED_FIELDS']}
    Run Keyword And Continue On Failure         Dictionary Should Contain Subdictionary     ${response_object_getaccesspassdetails.json()}                  ${expected_response_body_getaccesspassdetails}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['controlCode']}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['referenceId']}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['status']}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['validFrom']}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['validUntil']}
    [Teardown]              Run Keyword And Continue On Failure         RapidPassPH-Helpers.Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: DECLINED

getAccessPassDetails should return and reflect correct key-value pair from Single newRequestPass when passType is VEHICLE
    [Documentation]         Fails if HTTP status code is not 200.
    ...                     Fails if response body json key-value pair is not found.
    # Setup
    ${single_newRequestPass_details}=           RapidPassPH-Helpers.Generate Random Single newRequestPass Details       requester_type=VEHICLE
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass          aporType=${single_newRequestPass_details}[aporType]
    ...                                                                                                                                             company=${single_newRequestPass_details}[company]
    ...                                                                                                                                             destCity=${single_newRequestPass_details}[destCity]
    ...                                                                                                                                             destName=${single_newRequestPass_details}[destName]
    ...                                                                                                                                             destProvince=${single_newRequestPass_details}[destProvince]
    ...                                                                                                                                             destStreet=${single_newRequestPass_details}[destStreet]
    ...                                                                                                                                             email=${single_newRequestPass_details}[email]
    ...                                                                                                                                             firstName=${single_newRequestPass_details}[firstName]
    ...                                                                                                                                             idType=${single_newRequestPass_details}[idType]
    ...                                                                                                                                             identifierNumber=${single_newRequestPass_details}[identifierNumber]
    ...                                                                                                                                             lastName=${single_newRequestPass_details}[lastName]
    ...                                                                                                                                             middleName=${single_newRequestPass_details}[middleName]
    ...                                                                                                                                             mobileNumber=${MOBILENUMBER}
    ...                                                                                                                                             originCity=${single_newRequestPass_details}[originCity]
    ...                                                                                                                                             originName=${single_newRequestPass_details}[originName]
    ...                                                                                                                                             originProvince=${single_newRequestPass_details}[originProvince]
    ...                                                                                                                                             originStreet=${single_newRequestPass_details}[originStreet]
    ...                                                                                                                                             passType=${single_newRequestPass_details}[passType]
    ...                                                                                                                                             plateNumber=${single_newRequestPass_details}[plateNumber]
    ...                                                                                                                                             remarks=${single_newRequestPass_details}[remarks]
    ...                                                                                                                                             suffix=${single_newRequestPass_details}[suffix]
    # Main Test
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails           referenceId=${request_data_single_newrequestpass}[plateNumber]
    Run Keyword And Continue On Failure         Should Be Equal As Strings      200         ${response_object_getaccesspassdetails.status_code}
    ${expected_response_body_getaccesspassdetails}=                     Copy Dictionary     ${request_data_single_newrequestpass}
    Keep In Dictionary      ${expected_response_body_getaccesspassdetails}      @{RAPIDPASSPH_API_RULES['GETACCESSPASSDETAILS']['EXPECTED_FIELDS']}
    Run Keyword And Continue On Failure         Dictionary Should Contain Subdictionary     ${response_object_getaccesspassdetails.json()}                  ${expected_response_body_getaccesspassdetails}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['controlCode']}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['referenceId']}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['status']}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['validFrom']}
    Run Keyword And Continue On Failure         Should Not Be Empty     ${response_object_getaccesspassdetails.json()['validUntil']}
    [Teardown]              Run Keyword And Continue On Failure         RapidPassPH-Helpers.Set RapidPass With referenceId: ${request_data_single_newrequestpass}[plateNumber] To Valid Status: DECLINED

getAccessPassDetails should not return near-matched record
    [Documentation]         Fails if HTTP status code is not 404.
    [Setup]                 RapidPassPH-Helpers.Create INDIVIDUAL passType RapidPass Using mobileNumber: ${MOBILENUMBER}
    ${near_match_string}=   Get Substring       ${MOBILENUMBER}         start=              end=-3
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails           referenceId=${near_match_string}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      404         ${response_object_getaccesspassdetails.status_code}
    [Teardown]              Run Keyword And Continue On Failure         RapidPassPH-Helpers.Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: DECLINED

getAccessPassDetails should not return all records with * referenceId
    [Documentation]         Fails if HTTP status code is not 404.
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails           referenceId=*
    Run Keyword And Continue On Failure         Should Be Equal As Strings      404         ${response_object_getaccesspassdetails.status_code}
