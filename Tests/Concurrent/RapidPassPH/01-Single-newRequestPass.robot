*** Settings ***
Force Tags              References: JIRA-Ticket-ID
Resource                ${EXECDIR}${/}Resources${/}Common.robot

*** Test Cases ***

#Conent Validation Test Cases:
Single newRequestPass should not accept non-JSON
    [Documentation]         Fails if HTTP status code is not 415.
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    &{request_headers}=     Create Dictionary   Content-Type=application/x-www-form-urlencoded
    &{request_data}=        Create Dictionary   aporType=${EMPTY}
    ...                                         company=${EMPTY}
    ...                                         destCity=${EMPTY}
    ...                                         destName=${EMPTY}
    ...                                         destProvince=${EMPTY}
    ...                                         destStreet=${EMPTY}
    ...                                         email=${EMPTY}
    ...                                         firstName=${EMPTY}
    ...                                         idType=${EMPTY}
    ...                                         identifierNumber=${EMPTY}
    ...                                         lastName=${EMPTY}
    ...                                         middleName=${EMPTY}
    ...                                         mobileNumber=${EMPTY}
    ...                                         name=${EMPTY}
    ...                                         originCity=${EMPTY}
    ...                                         originName=${EMPTY}
    ...                                         originProvince=${EMPTY}
    ...                                         originStreet=${EMPTY}
    ...                                         passType=${EMPTY}
    ...                                         plateNumber=${EMPTY}
    ...                                         refNum=${EMPTY}
    ...                                         remarks=${EMPTY}
    ...                                         suffix=${EMPTY}
    ${response_object_single_newrequestpass}=   Post Request        rapidpassph      /registry/access-passes
    ...                                         headers=${request_headers}
    ...                                         data=${request_data}
    Log                     ${response_object_single_newrequestpass.content}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      415         ${response_object_single_newrequestpass.status_code}

Single newRequestPass should not accept empty JSON
    [Documentation]         Fails if HTTP status code is not 400.
    ...                     Fails if response body is not as expected when request body is empty JSON.
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    &{request_headers}=     Create Dictionary   Content-Type=application/json
    &{request_data}=        Create Dictionary
    ${response_object_single_newrequestpass}=   Post Request        rapidpassph      /registry/access-passes
    ...                                         headers=${request_headers}
    ...                                         data=${request_data}
    Log                     ${response_object_single_newrequestpass.content}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_single_newrequestpass.status_code}
    &{expected_response_body}=                  Create Dictionary
    FOR     ${required_field}       IN          @{RAPIDPASSPH_API_RULES['SINGLE_NEWREQUESTPASS']['REQUIRED_FIELDS']}
            Set To Dictionary       ${expected_response_body}                   ${required_field}=${RAPIDPASSPH_API_SPIELS['ERR']['REQUIRED_FIELD']}
    END
    Run Keyword And Continue On Failure         Dictionaries Should Be Equal    ${expected_response_body}       ${response_object_single_newrequestpass.json()}

Single newRequestPass null passType should be caught
    [Documentation]         Fails if HTTP status code is not 400.
    ...                     Fails if response body is not as expected when passType is null.
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass      passType=${null}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_single_newrequestpass.status_code}
    &{expected_response_body}=                  Create Dictionary               passType=${RAPIDPASSPH_API_SPIELS['ERR']['NULL_FIELD']}
    Run Keyword And Continue On Failure         Dictionaries Should Be Equal    ${expected_response_body}       ${response_object_single_newrequestpass.json()}

Single newRequestPass should accept valid passType value: INDIVIDUAL
    [Documentation]         Fails if HTTP status code is not 201.
    ...                     Fails if response body does not contain correct referenceId item.
    ...
    ...                     kapag `passType`: `INDIVIDUAL`
    ...                     ang `referenceId` ay `mobileNumber`
    ...                     ok lang kahit walang `plateNumber` field
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass      passType=INDIVIDUAL
    Run Keyword And Continue On Failure         Should Be Equal As Strings      201         ${response_object_single_newrequestpass.status_code}
    ${referenceId}=         RapidPassPH-Helpers.Extract referenceId from Single newRequestPass          ${response_object_single_newrequestpass}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      ${request_data_single_newrequestpass['mobileNumber']}           ${referenceId}
    [Teardown]              RapidPassPH-Helpers.Set RapidPass With referenceId: ${request_data_single_newrequestpass['mobileNumber']} To Valid Status: DECLINED

Single newRequestPass should accept valid passType value: VEHICLE
    [Documentation]         Fails if HTTP status code is not 201.
    ...                     Fails if response body does not contain correct referenceId item.
    ...
    ...                     kapag `passType`: `VEHICLE`
    ...                     ang `referenceId` ay `plateNumber`
    ...                     mageerror kapag walang `plateNumber` field
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass      aporType=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['APORTYPE']}
    ...                                                                                                                                         company=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['COMPANY']}
    ...                                                                                                                                         destCity=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['DESTCITY']}
    ...                                                                                                                                         destName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['DESTNAME']}
    ...                                                                                                                                         destProvince=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['DESTPROVINCE']}
    ...                                                                                                                                         destStreet=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['DESTSTREET']}
    ...                                                                                                                                         email=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['EMAIL']}
    ...                                                                                                                                         firstName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['FIRSTNAME']}
    ...                                                                                                                                         idType=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['IDTYPE']}
    ...                                                                                                                                         identifierNumber=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['IDENTIFIERNUMBER']}
    ...                                                                                                                                         lastName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['LASTNAME']}
    ...                                                                                                                                         middleName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['MIDDLENAME']}
    ...                                                                                                                                         mobileNumber=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['MOBILENUMBER']}
    ...                                                                                                                                         originCity=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['ORIGINCITY']}
    ...                                                                                                                                         originName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['ORIGINNAME']}
    ...                                                                                                                                         originProvince=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['ORIGINPROVINCE']}
    ...                                                                                                                                         originStreet=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['ORIGINSTREET']}
    ...                                                                                                                                         passType=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['PASSTYPE']}
    ...                                                                                                                                         plateNumber=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['PLATENUMBER']}
    ...                                                                                                                                         remarks=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['REMARKS']}
    ...                                                                                                                                         suffix=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['SUFFIX']}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      201         ${response_object_single_newrequestpass.status_code}
    ${referenceId}=         RapidPassPH-Helpers.Extract referenceId from Single newRequestPass          ${response_object_single_newrequestpass}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      ${request_data_single_newrequestpass['plateNumber']}           ${referenceId}
    [Teardown]              RapidPassPH-Helpers.Set RapidPass With referenceId: ${request_data_single_newrequestpass['plateNumber']} To Valid Status: DECLINED

Single newRequestPass should not accept non-valid passType values
    [Documentation]         Fails if HTTP status code is not 400.
    ${random_valid_pass_type}=      Common.Pick Randomly From Arguments     @{RAPIDPASSPH_SELECTION_VALUES_PASSTYPE}
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass      passType=${random_valid_pass_type}*
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_single_newrequestpass.status_code}

Single newRequestPass should accept any free-form aporType strings specifically from selection values
    [Documentation]         Fails if HTTP status code is not 201.
    ...                     Fails if response body does not contain correct referenceId item.
    FOR     ${aporType}     IN      @{RAPIDPASSPH_SELECTION_VALUES_APORTYPE}
            ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass      aporType=${aporType}
            Run Keyword And Continue On Failure         Should Be Equal As Strings      201         ${response_object_single_newrequestpass.status_code}
            ${referenceId}=     RapidPassPH-Helpers.Extract referenceId from Single newRequestPass          ${response_object_single_newrequestpass}
            Run Keyword And Continue On Failure         Should Be Equal As Strings      ${request_data_single_newrequestpass['mobileNumber']}           ${referenceId}
            # Teardown
            Run Keyword And Continue On Failure         RapidPassPH-Helpers.Set RapidPass With referenceId: ${request_data_single_newrequestpass['mobileNumber']} To Valid Status: DECLINED
    END

Single newRequestPass should not accept null mobileNumber when passType is INDIVIDUAL
    [Documentation]         Fails if HTTP status code is not 400.
    ...                     Fails if response body does not contain correct error message item.
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass      passType=INDIVIDUAL
    ...                                                                                                                                         mobileNumber=${null}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_single_newrequestpass.status_code}
    &{expected_response_body}=                  Create Dictionary               mobileNumber=${RAPIDPASSPH_API_SPIELS['ERR']['NULL_FIELD']}
    Run Keyword And Continue On Failure         Dictionaries Should Be Equal    ${expected_response_body}       ${response_object_single_newrequestpass.json()}

Single newRequestPass should not accept null plateNumber when passType is VEHICLE
    [Documentation]         Fails if HTTP status code is not 400.
    ...                     Fails if response body does not contain correct error message item.
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass      aporType=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['APORTYPE']}
    ...                                                                                                                                         company=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['COMPANY']}
    ...                                                                                                                                         destCity=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['DESTCITY']}
    ...                                                                                                                                         destName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['DESTNAME']}
    ...                                                                                                                                         destProvince=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['DESTPROVINCE']}
    ...                                                                                                                                         destStreet=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['DESTSTREET']}
    ...                                                                                                                                         email=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['EMAIL']}
    ...                                                                                                                                         firstName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['FIRSTNAME']}
    ...                                                                                                                                         idType=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['IDTYPE']}
    ...                                                                                                                                         identifierNumber=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['IDENTIFIERNUMBER']}
    ...                                                                                                                                         lastName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['LASTNAME']}
    ...                                                                                                                                         middleName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['MIDDLENAME']}
    ...                                                                                                                                         mobileNumber=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['MOBILENUMBER']}
    ...                                                                                                                                         originCity=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['ORIGINCITY']}
    ...                                                                                                                                         originName=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['ORIGINNAME']}
    ...                                                                                                                                         originProvince=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['ORIGINPROVINCE']}
    ...                                                                                                                                         originStreet=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['ORIGINSTREET']}
    ...                                                                                                                                         passType=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['PASSTYPE']}
    ...                                                                                                                                         plateNumber=${null}
    ...                                                                                                                                         remarks=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['REMARKS']}
    ...                                                                                                                                         suffix=${RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE['SUFFIX']}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_single_newrequestpass.status_code}
    &{expected_response_body}=                  Create Dictionary               plateNumber=${RAPIDPASSPH_API_SPIELS['ERR']['NULL_FIELD']}
    Run Keyword And Continue On Failure         Dictionaries Should Be Equal    ${expected_response_body}       ${response_object_single_newrequestpass.json()}
