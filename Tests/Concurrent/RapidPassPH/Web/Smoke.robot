*** Settings ***
Force Tags      References: JIRA-Ticket-ID
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Test Setup      Common.Begin Test   ${RAPIDPASSPH_WEB_URL}
Test Teardown   Common.End Test

*** Test Cases ***
Valid Apply For Rapid Pass - Individual
    [Documentation]             Valid Apply for Rapid Pass for an individual
    [Tags]                      smoke   individual  register
    Begin Apply For Pass
    Proceed With Onboarding
    ${requester_data}=          Generate Random Single newRequestPass Details
    ${id_type}=                 Common.Pick Randomly From Arguments             @{RAPIDPASSPH_WEB_SELECTION_VALUES_ID_TYPE}
    ${nature_of_work_pair}=     Common.Pick Randomly From Arguments             @{RAPIDPASSPH_SELECTION_VALUES_APORTYPE_WITH_DESCRIPTION}
    ${nature_of_work}=          Common.Get Value From Pair                      pair=${nature_of_work_pair}
    Perform Application         firstName=${requester_data}[firstName]
    ...                         lastName=${requester_data}[lastName]
    ...                         email=${requester_data}[email]
    ...                         mobileNumber=${requester_data}[mobileNumber]
    ...                         company=${requester_data}[company]
    ...                         idType=${id_type}
    ...                         idNumber=${requester_data}[identifierNumber]
    ...                         originStreet=${requester_data}[originStreet]
    ...                         originCity=${requester_data}[originCity]
    ...                         originProvince=${requester_data}[originProvince]
    ...                         destStreet=${requester_data}[destStreet]
    ...                         destCity=${requester_data}[destCity]
    ...                         destProvince=${requester_data}[destProvince]
    ...                         aporType=${nature_of_work}
    ...                         tos=${TRUE}
    Wait For Application Result Page
    Page Should Contain         ${SPIEL_SUBMIT_SUCCESS}
    ${application_status}=      Get Application Status
    Should Be Equal As Strings  ${application_status}                           ${NEW_APPLICATION_STATUS}

Valid Apply For Rapid Pass - Vehicle
    [Documentation]             Valid Apply for Rapid Pass for a vehicle
    [Tags]                      smoke   vehicle     register
    Begin Apply For Pass
    Proceed With Onboarding     vehicle
    ${requester_data}=          Generate Random Single newRequestPass Details   vehicle
    ${id_type}=                 Common.Pick Randomly From Arguments             @{RAPIDPASSPH_WEB_SELECTION_VALUES_ID_TYPE}
    ${nature_of_work_pair}=     Common.Pick Randomly From Arguments             @{RAPIDPASSPH_SELECTION_VALUES_APORTYPE_WITH_DESCRIPTION}
    ${nature_of_work}=          Common.Get Value From Pair                      pair=${nature_of_work_pair}
    ${vehicle_id_type}=         Common.Pick Randomly From Arguments             @{RAPIDPASSPH_WEB_SELECTION_VALUES_VEHICLE_ID_TYPE}
    Perform Application         applicationType=vehicle
    ...                         firstName=${requester_data}[firstName]
    ...                         lastName=${requester_data}[lastName]
    ...                         email=${requester_data}[email]
    ...                         mobileNumber=${requester_data}[mobileNumber]
    ...                         company=${requester_data}[company]
    ...                         idNumber=${requester_data}[identifierNumber]
    ...                         originStreet=${requester_data}[originStreet]
    ...                         originCity=${requester_data}[originCity]
    ...                         originProvince=${requester_data}[originProvince]
    ...                         destStreet=${requester_data}[destStreet]
    ...                         destCity=${requester_data}[destCity]
    ...                         destProvince=${requester_data}[destProvince]
    ...                         aporType=${nature_of_work}
    ...                         vehicleIdType=${vehicle_id_type}
    ...                         vehicleId=${requester_data}[plateNumber]
    ...                         tos=${TRUE}
    Wait For Application Result Page
    Page Should Contain         ${SPIEL_SUBMIT_SUCCESS}
    ${application_status}=      Get Application Status
    Should Be Equal As Strings  ${application_status}                           ${NEW_APPLICATION_STATUS}