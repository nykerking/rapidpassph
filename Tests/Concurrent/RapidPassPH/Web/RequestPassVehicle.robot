*** Settings ***
Force Tags      References: JIRA-Ticket-ID
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Common.Begin Test   ${RAPIDPASSPH_WEB_URL}
Test Setup      Run Keywords        Go To   ${RAPIDPASSPH_WEB_URL}
...             AND                 Prepare Data
Suite Teardown  Common.End Test

*** Keywords ***
Prepare Data
    Begin Apply For Pass
    Proceed With Onboarding     vehicle
    ${REQUESTER_DATA}=          Generate Random Single newRequestPass Details
    ${ID_TYPE}=                 Common.Pick Randomly From Arguments     @{RAPIDPASSPH_WEB_SELECTION_VALUES_ID_TYPE}
    ${nature_of_work_pair}=     Common.Pick Randomly From Arguments     @{RAPIDPASSPH_SELECTION_VALUES_APORTYPE_WITH_DESCRIPTION}
    ${NATURE_OF_WORK}=          Common.Get Value From Pair              pair=${nature_of_work_pair}
    ${VEHICLE_ID_TYPE}=         Common.Pick Randomly From Arguments     @{RAPIDPASSPH_WEB_SELECTION_VALUES_VEHICLE_ID_TYPE}
    Set Suite Variable          ${REQUESTER_DATA}
    Set Suite Variable          ${ID_TYPE}
    Set Suite Variable          ${NATURE_OF_WORK}
    Set Suite Variable          ${VEHICLE_ID_TYPE}

*** Test Cases ***
Register With Blank Vehicle ID Type
    [Documentation]             Creates a vehicle access pass application with blank vehicle ID Type
    [Tags]                      negative   vehicle  register
    Perform Application         applicationType=vehicle
    ...                         firstName=${requester_data}[firstName]
    ...                         lastName=${requester_data}[lastName]
    ...                         email=${requester_data}[email]
    ...                         mobileNumber=${requester_data}[mobileNumber]
    ...                         company=${requester_data}[company]
    ...                         idNumber=${requester_data}[identifierNumber]
    ...                         originStreet=${requester_data}[originStreet]
    ...                         originCity=${requester_data}[originCity]
    ...                         originProvince=${requester_data}[originProvince]
    ...                         destStreet=${requester_data}[destStreet]
    ...                         destCity=${requester_data}[destCity]
    ...                         destProvince=${requester_data}[destProvince]
    ...                         aporType=${nature_of_work}
    ...                         vehicleIdType=${EMPTY}
    ...                         vehicleId=${requester_data}[plateNumber]
    ...                         tos=${TRUE}
    Page Should Contain         ${FORM_PAGE_TITLE}
    Assert Input Error          Vehicle ID Type                         ${SPIEL_BLANK_FIELD}
Register With Blank Vehicle ID
    [Documentation]             Creates a vehicle access pass application with blank vehicle ID
    [Tags]                      negative   vehicle  register
    Perform Application         applicationType=vehicle
    ...                         firstName=${requester_data}[firstName]
    ...                         lastName=${requester_data}[lastName]
    ...                         email=${requester_data}[email]
    ...                         mobileNumber=${requester_data}[mobileNumber]
    ...                         company=${requester_data}[company]
    ...                         idNumber=${requester_data}[identifierNumber]
    ...                         originStreet=${requester_data}[originStreet]
    ...                         originCity=${requester_data}[originCity]
    ...                         originProvince=${requester_data}[originProvince]
    ...                         destStreet=${requester_data}[destStreet]
    ...                         destCity=${requester_data}[destCity]
    ...                         destProvince=${requester_data}[destProvince]
    ...                         aporType=${nature_of_work}
    ...                         vehicleIdType=${VEHICLE_ID_TYPE}
    ...                         vehicleId=${EMPTYs}
    ...                         tos=${TRUE}
    Page Should Contain         ${FORM_PAGE_TITLE}
    Assert Input Error          Vehicle ID                              ${SPIEL_BLANK_FIELD}