*** Settings ***
Force Tags      References: JIRA-Ticket-ID
Resource        ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup     Common.Begin Test   ${RAPIDPASSPH_WEB_URL}
Test Setup      Run Keywords        Go To   ${RAPIDPASSPH_WEB_URL}
...             AND                 Prepare Data
Suite Teardown  Common.End Test

*** Keywords ***
Prepare Data
    Begin Apply For Pass
    Proceed With Onboarding
    ${REQUESTER_DATA}=          Generate Random Single newRequestPass Details
    ${ID_TYPE}=                 Common.Pick Randomly From Arguments
    ...                         @{RAPIDPASSPH_WEB_SELECTION_VALUES_ID_TYPE}
    ${nature_of_work_pair}=     Common.Pick Randomly From Arguments
    ...                         @{RAPIDPASSPH_SELECTION_VALUES_APORTYPE_WITH_DESCRIPTION}
    ${NATURE_OF_WORK}=          Common.Get Value From Pair
    ...                         pair=${nature_of_work_pair}
    Set Suite Variable          ${REQUESTER_DATA}
    Set Suite Variable          ${ID_TYPE}
    Set Suite Variable          ${NATURE_OF_WORK}

*** Test Cases ***
Register With Blank First Name
    [Documentation]     Creates an individual access pass application with blank first name
    [Tags]  negative   individual  register
    Perform Application     firstName=${EMPTY}
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      First Name  ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Format First Name
    [Documentation]     Creates an individual access pass application with invalid format first name
    [Tags]  negative   individual  register
    Perform Application     firstName=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     First Name
    Assert Input Error      First Name  ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space First Name
    [Documentation]     Creates an individual access pass application with space input for first name
    [Tags]  negative   individual  register
    Perform Application     firstName=${SPACE}
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      First Name  ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length First Name
    [Documentation]     Creates an individual access pass application with first name > 50 chars long
    [Tags]  negative   individual  register
    ${first_name}=  FakerLibrary.Random Letters     51
    Perform Application     firstName=${first_name}
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      First Name  ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Last Name
    [Documentation]     Creates an individual access pass application with blank last name
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${EMPTY}
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Last Name   ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Format Last Name
    [Documentation]     Creates an individual access pass application with invalid format last name
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     Last Name
    Assert Input Error      Last Name   ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Numeric Last Name
    [Documentation]     Creates an individual access pass application with numeric last name
    [Tags]  negative   individual  register
    ${last_name}=   FakerLibrary.Random Number
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${last_name}
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Last Name   ${SPIEL_NUMERIC_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Last Name
    [Documentation]     Creates an individual access pass application with space input for last name
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${SPACE}
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Last Name   ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Last Name
    [Documentation]     Creates an individual access pass application with last name > 51 chars long
    [Tags]  negative   individual  register
    ${last_name}=   FakerLibrary.Random Letters     51
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${last_name}
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Last Name   ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Email
    [Documentation]     Creates an individual access pass application with blank email
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${EMPTY}
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Email   ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Email
    [Documentation]     Creates an individual access pass application with space input for email
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${SPACE}
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Email   ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Format Email
    [Documentation]     Creates an individual access pass application with invalid format email
    [Tags]  negative   individual  register
    ${email}=   FakerLibrary.Random Number
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${email}
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_VALUE}  $   email
    Assert Input Error      Email   ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Email
    [Documentation]     Creates an individual access pass application with email > 100 characters long
    [Tags]  negative   individual  register
    ${invalid_email}=       FakerLibrary.Random Letters     95
    ${invalid_email}=       Catenate    SEPARATOR=  ${invalid_email}    @q.com
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${invalid_email}
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Email   ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Mobile Number
    [Documentation]     Creates an individual access pass application with blank mobile number
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${EMPTY}
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Mobile Number   ${SPIEL_BLANK_FIELD}
    ...                     errorSpiel=${SPIEL_FIELD_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Mobile Number
    [Documentation]     Creates an individual access pass application with space input for mobile number
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${SPACE}
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Mobile Number   ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Non-numeric Mobile Number
    [Documentation]     Creates an individual access pass application with non-numeric mobile number
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_VALUE}  $
    ...                     mobile number
    Assert Input Error      Mobile Number   ${fieldErrorLabel}
    ...                     errorSpiel=${SPIEL_FIELD_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Short Mobile Number
    [Documentation]     Creates an individual access pass application with mobile number < 11 digits long
    [Tags]  negative   individual  register
    ${mobile_number}=   FakerLibrary.Random Number  10  ${TRUE}
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${mobile_number}
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_VALUE}  $
    ...                     mobile number
    Assert Input Error      Mobile Number   ${fieldErrorLabel}
    ...                     errorSpiel=${SPIEL_FIELD_ERROR}

Register With Long Mobile Number
    [Documentation]     Creates an individual access pass application with mobile number > 11 digits long
    [Tags]  negative   individual  register
    ${mobile_number}=   FakerLibrary.Random Number  12  ${TRUE}
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${mobile_number}
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_VALUE}  $
    ...                     mobile number
    Assert Input Error      Mobile Number   ${fieldErrorLabel}
    ...                     errorSpiel=${SPIEL_FIELD_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Company
    [Documentation]     Creates an individual access pass application with blank company
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${EMPTY}
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Company/Institution Name    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Company
    [Documentation]     Creates an individual access pass application with space input for company
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${SPACE}
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Company/Institution Name    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Format Company
    [Documentation]     Creates an individual access pass application with invalid special characters for company
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     Company
    Assert Input Error      Company     ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Company
    [Documentation]     Creates an individual access pass application with company > 100 characters long
    [Tags]  negative   individual  register
    ${company}=     FakerLibrary.Random Letters     101
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${company}
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Company/Institution Name    ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank ID Type
    [Documentation]     Creates an individual access pass application with blank ID type
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${EMPTY}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      ID Type     ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank ID Number
    [Documentation]     Creates an individual access pass application with blank ID number
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${EMPTY}
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      ID Number   ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space ID Number
    [Documentation]     Creates an individual access pass application with space input for ID number
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${SPACE}
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      ID Number   ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Format ID Number
    [Documentation]     Creates an individual access pass application with ID number containing characters other than numbers and dashes
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      ID Number   ${SPIEL_ID_NUMBER_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length ID Number
    [Documentation]     Creates an individual access pass application with ID number > 50 characters long
    [Tags]  negative   individual  register
    ${id_number}=   FakerLibrary.Random Number  51   ${TRUE}
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${id_number}
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      ID Number   ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}


Register With Blank Origin Street
    [Documentation]     Creates an individual access pass application with blank origin street
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${EMPTY}
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Street  ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Origin Street
    [Documentation]     Creates an individual access pass application with space input for origin street
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${SPACE}
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Street  ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Format Origin Street
    [Documentation]     Creates an individual access pass application with origin street containing invalid characters
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     Street
    Assert Input Error      Street  ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Origin Street
    [Documentation]     Creates an individual access pass application with origin street > 50 characters long
    [Tags]  negative   individual  register
    ${origin_street}=   FakerLibrary.Random Letters     51
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${origin_street}
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Street  ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Origin City
    [Documentation]     Creates an individual access pass application with blank origin city
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${EMPTY}
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      City    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Origin City
    [Documentation]     Creates an individual access pass application with space input for origin city
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${SPACE}
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      City    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Format Origin City
    [Documentation]     Creates an individual access pass application with origin city containing invalid characters
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     City
    Assert Input Error      City    ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Origin City
    [Documentation]     Creates an individual access pass application with origin city > 50 characters
    [Tags]  negative   individual  register
    ${origin_city}=     FakerLibrary.Random Letters     51
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${origin_city}
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      City    ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Origin Province
    [Documentation]     Creates an individual access pass application with blank origin province
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${EMPTY}
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Province    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Origin Province
    [Documentation]     Creates an individual access pass application with space input for origin province
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${SPACE}
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Province    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Origin Province
    [Documentation]     Creates an individual access pass application with invalid characters for origin province
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     Province
    Assert Input Error      Province    ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Origin Province
    [Documentation]     Creates an individual access pass application with input > 50 characters for origin province
    [Tags]  negative   individual  register
    ${origin_province}=     FakerLibrary.Random Letters     51
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${origin_province}
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Province    ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Destination Street
    [Documentation]     Creates an individual access pass application with blank destination street
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${EMPTY}
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Street  ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Destination Street
    [Documentation]     Creates an individual access pass application with space input for destination street
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${SPACE}
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Street  ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Destination Street
    [Documentation]     Creates an individual access pass application with invalid character input for destination street
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     Street
    Assert Input Error      Street  ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Destination Street
    [Documentation]     Creates an individual access pass application with input > 50 characters for destination street
    [Tags]  negative   individual  register
    ${destination_street}=  FakerLibrary.Random Letters     51
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${destination_street}
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Street  ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Destination City
    [Documentation]     Creates an individual access pass application with blank destination city
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${EMPTY}
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      City    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Destination City
    [Documentation]     Creates an individual access pass application with space input for destination city
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${SPACE}
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      City    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Destination City
    [Documentation]     Creates an individual access pass application with invalid character input for destination city
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     City
    Assert Input Error      City    ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Destination City
    [Documentation]     Creates an individual access pass application with input > 50 characters for destination city
    [Tags]  negative   individual  register
    ${destination_city}=    FakerLibrary.Random Letters     51
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${destination_city}
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      City    ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Destination Province
    [Documentation]     Creates an individual access pass application with blank destination province
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${EMPTY}
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Province    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Space Destination Province
    [Documentation]     Creates an individual access pass application with space input for destination province
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${SPACE}
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Province    ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Destination Province
    [Documentation]     Creates an individual access pass application with invalid character input for destination province
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REGISTRATION_INPUT['INVALID_FORMAT_NAME']}
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    ${fieldErrorLabel}=     Replace String  ${SPIEL_INVALID_FORMAT}     $
    ...                     Province
    Assert Input Error      Province    ${fieldErrorLabel}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Invalid Length Destination Province
    [Documentation]     Creates an individual access pass application with input > 50 characters for destination province
    [Tags]  negative   individual  register
    ${destination_province}=    FakerLibrary.Random Letters     51
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${destination_province}
    ...                     aporType=${NATURE_OF_WORK}
    ...                     tos=${TRUE}
    Assert Input Error      Province    ${SPIEL_LENGTH_ERROR}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Blank Nature Of Work
    [Documentation]     Creates an individual access pass application with blank nature of work
    [Tags]  negative   individual  register
    Perform Application     firstName=${REQUESTER_DATA}[firstName]
    ...                     lastName=${REQUESTER_DATA}[lastName]
    ...                     email=${REQUESTER_DATA}[email]
    ...                     mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                     company=${REQUESTER_DATA}[company]
    ...                     idType=${ID_TYPE}
    ...                     idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                     originStreet=${REQUESTER_DATA}[originStreet]
    ...                     originCity=${REQUESTER_DATA}[originCity]
    ...                     originProvince=${REQUESTER_DATA}[originProvince]
    ...                     destStreet=${REQUESTER_DATA}[destStreet]
    ...                     destCity=${REQUESTER_DATA}[destCity]
    ...                     destProvince=${REQUESTER_DATA}[destProvince]
    ...                     aporType=${EMPTY}
    ...                     tos=${TRUE}
    Assert Input Error      Nature of Work  ${SPIEL_BLANK_FIELD}
    Page Should Contain Element     ${BTN_SUBMIT}

Register With Unchecked Terms And Conditions
    [Documentation]     Creates an individual access pass application with unchecked terms and conditions
    [Tags]  negative   individual  register
    Perform Application         firstName=${REQUESTER_DATA}[firstName]
    ...                         lastName=${REQUESTER_DATA}[lastName]
    ...                         email=${REQUESTER_DATA}[email]
    ...                         mobileNumber=${REQUESTER_DATA}[mobileNumber]
    ...                         company=${REQUESTER_DATA}[company]
    ...                         idType=${ID_TYPE}
    ...                         idNumber=${REQUESTER_DATA}[identifierNumber]
    ...                         originStreet=${REQUESTER_DATA}[originStreet]
    ...                         originCity=${REQUESTER_DATA}[originCity]
    ...                         originProvince=${REQUESTER_DATA}[originProvince]
    ...                         destStreet=${REQUESTER_DATA}[destStreet]
    ...                         destCity=${REQUESTER_DATA}[destCity]
    ...                         destProvince=${REQUESTER_DATA}[destProvince]
    ...                         aporType=${NATURE_OF_WORK}
    ...                         tos=${FALSE}
    Element Should Be Disabled  ${BTN_SUBMIT}
