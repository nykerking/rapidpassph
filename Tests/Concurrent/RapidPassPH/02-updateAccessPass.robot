*** Settings ***
Force Tags              References: JIRA-Ticket-ID
Resource                ${EXECDIR}${/}Resources${/}Common.robot

*** Variables ***
${MOBILENUMBER}         ${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['REFERENCEID']}

*** Test Cases ***

#Conent Validation Test Cases:
updateAccessPass should not accept non-JSON
    [Documentation]         Fails if HTTP status code is not 415.
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    &{request_headers}=     Create Dictionary   Content-Type=application/x-www-form-urlencoded
    &{request_data}=        Create Dictionary   status=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['STATUS']}
    ${response_object_updateaccesspass}=        Put Request        rapidpassph      /registry/access-passes/${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['REFERENCEID']}
    ...                                         headers=${request_headers}
    ...                                         data=${request_data}
    Log                     ${response_object_updateaccesspass.content}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      415         ${response_object_updateaccesspass.status_code}

updateAccessPass should not accept empty JSON
    [Documentation]         Fails if HTTP status code is not 400.
    ...                     Fails if response body is not as expected when request body is empty JSON.
    Create Session          rapidpassph         ${RAPIDPASSPH_API_URL_WHOLE}                verify=${True}
    &{request_headers}=     Create Dictionary   Content-Type=application/json
    &{request_data}=        Create Dictionary
    ${response_object_updateaccesspass}=        Put Request        rapidpassph      /registry/access-passes/${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['REFERENCEID']}
    ...                                         headers=${request_headers}
    ...                                         data=${request_data}
    Log                     ${response_object_updateaccesspass.content}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_updateaccesspass.status_code}
    &{expected_response_body}=                  Create Dictionary
    FOR     ${required_field}       IN          @{RAPIDPASSPH_API_RULES['UPDATEACCESSPASS']['REQUIRED_FIELDS']}
            Set To Dictionary       ${expected_response_body}                   ${required_field}=${RAPIDPASSPH_API_SPIELS['ERR']['REQUIRED_FIELD']}
    END
    Run Keyword And Continue On Failure         Dictionaries Should Be Equal    ${expected_response_body}       ${response_object_updateaccesspass.json()}

updateAccessPass null status should be caught
    [Documentation]         Fails if HTTP status code is not 400.
    ...                     Fails if response body is not as expected when status is null.
    ${response_object_updateaccesspass}         ${request_data_updatepass}=     RapidPassPH-API-Requests.updateAccessPass                       referenceId=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['REFERENCEID']}
    ...                                                                                                                                         status=${null}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_updateaccesspass.status_code}
    &{expected_response_body}=                  Create Dictionary               status=${RAPIDPASSPH_API_SPIELS['ERR']['NULL_FIELD']}
    Run Keyword And Continue On Failure         Dictionaries Should Be Equal    ${expected_response_body}       ${response_object_updateaccesspass.json()}

updateAccessPass should not accept non-valid values
    [Documentation]         Fails if HTTP status code is not 400.
    ...                     Fails if response body is not as expected when status is invalid.
    ${response_object_updateaccesspass}         ${request_data_updatepass}=     RapidPassPH-API-Requests.updateAccessPass                       referenceId=${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['REFERENCEID']}
    ...                                                                                                                                         status=DENIED
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_updateaccesspass.status_code}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      ${RAPIDPASSPH_API_SPIELS['ERR']['INVALID_STATUS_NAME']}         ${response_object_updateaccesspass.text}

updateAccessPass should accept valid status value: APPROVED
    [Documentation]         Fails if HTTP status code is not 200.
    ...                     Fails if response body does not contain updated status value.
    [Setup]                 RapidPassPH-Helpers.Create INDIVIDUAL passType RapidPass Using mobileNumber: ${MOBILENUMBER}
    Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: APPROVED
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails       ${MOBILENUMBER}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_getaccesspassdetails.json()}      status                  APPROVED
    [Teardown]              Run Keywords        Run Keyword If Test Passed      RapidPassPH-Helpers.Revoke RapidPass With referenceId: ${MOBILENUMBER}
    ...                         AND             Run Keyword If Test Failed      RapidPassPH-Helpers.Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: DECLINED

updateAccessPass should accept valid status value: DECLINED
    [Documentation]         Fails if HTTP status code is not 200.
    ...                     Fails if response body does not contain updated status value.
    [Setup]                 RapidPassPH-Helpers.Create INDIVIDUAL passType RapidPass Using mobileNumber: ${MOBILENUMBER}
    Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: DECLINED
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails       ${MOBILENUMBER}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_getaccesspassdetails.json()}      status                  DECLINED
    [Teardown]              Run Keyword If Test Failed      RapidPassPH-Helpers.Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: DECLINED
