*** Settings ***
Force Tags      References: JIRA-Ticket-ID
Resource        ${EXECDIR}${/}Resources${/}Common.robot

*** Variables ***
${MOBILENUMBER}         ${RAPIDPASSPH_DEFAULT_VALUES_GETACCESSPASSES['MOBILENUMBER']}

*** Test Cases ***
getAccessPasses should return and reflect correct key-value pair as from getAcessPassDetails
    [Documentation]         Fails if HTTP status code is not 200.
    ...                     Fails if response body json key-value pair is not found.
    [Setup]                 RapidPassPH-Helpers.Create INDIVIDUAL passType RapidPass Using mobileNumber: ${MOBILENUMBER}
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails           referenceId=${MOBILENUMBER}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      200         ${response_object_getaccesspassdetails.status_code}
    ${response_object_getaccesses}=             RapidPassPH-API-Requests.getAccessPasses
    List Should Contain Value                   ${response_object_getaccesses.json()}       ${response_object_getaccesspassdetails.json()}
    [Teardown]              Run Keyword And Continue On Failure         RapidPassPH-Helpers.Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: DECLINED
