*** Settings ***
Force Tags              References: JIRA-Ticket-ID
Resource                ${EXECDIR}${/}Resources${/}Common.robot
Suite Setup             RapidPassPH-Helpers.Ensure RapidPass Can Be Recreated               ${MOBILENUMBER}

*** Variables ***
${MOBILENUMBER}         ${RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS['REFERENCEID']}

*** Test Cases ***
Update status from PENDING to PENDING should not be allowed
    [Documentation]         Fails if HTTP status code is not 400.
    [SETUP]                 Create INDIVIDUAL passType RapidPass Using mobileNumber: ${MOBILENUMBER}
    ${response_object_updateaccesspass}         ${request_data_updateaccesspass}=           RapidPassPH-API-Requests.updateAccessPass           ${MOBILENUMBER}         status=PENDING
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_updateaccesspass.status_code}
    [Teardown]              RapidPassPH-Helpers.Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: DECLINED

Update status from PENDING to APPROVED should be allowed
    [Documentation]         Fails if HTTP status code is not 400.
    [SETUP]                 Create INDIVIDUAL passType RapidPass Using mobileNumber: ${MOBILENUMBER}
    ${response_object_updateaccesspass}         ${request_data_updateaccesspass}=           RapidPassPH-API-Requests.updateAccessPass           ${MOBILENUMBER}         status=APPROVED
    Run Keyword And Continue On Failure         Should Be Equal As Strings      200         ${response_object_updateaccesspass.status_code}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_updateaccesspass.json()}          status                  APPROVED
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails       ${MOBILENUMBER}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_getaccesspassdetails.json()}      status                  APPROVED
    [Teardown]              RapidPassPH-Helpers.Revoke RapidPass With referenceId: ${MOBILENUMBER}

Update status from PENDING to DECLINED should be allowed
    [Documentation]         Fails if HTTP status code is not 400.
    [SETUP]                 Create INDIVIDUAL passType RapidPass Using mobileNumber: ${MOBILENUMBER}
    ${response_object_updateaccesspass}         ${request_data_updateaccesspass}=           RapidPassPH-API-Requests.updateAccessPass           ${MOBILENUMBER}         status=DECLINED
    Run Keyword And Continue On Failure         Should Be Equal As Strings      200         ${response_object_updateaccesspass.status_code}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_updateaccesspass.json()}          status                  DECLINED
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails       ${MOBILENUMBER}
    Run Keyword And Continue On Failure         Dictionary Should Contain Item              ${response_object_getaccesspassdetails.json()}      status                  DECLINED

Update status from PENDING to SUSPENDED should be allowed
    [Documentation]         Fails if HTTP status code is not 400.
    [SETUP]                 Create INDIVIDUAL passType RapidPass Using mobileNumber: ${MOBILENUMBER}
    ${response_object_updateaccesspass}         ${request_data_updateaccesspass}=           RapidPassPH-Helpers.Revoke RapidPass With referenceId: ${MOBILENUMBER}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      400         ${response_object_updateaccesspass.status_code}
    [Teardown]              RapidPassPH-Helpers.Set RapidPass With referenceId: ${MOBILENUMBER} To Valid Status: DECLINED
