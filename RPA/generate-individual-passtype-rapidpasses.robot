*** Settings ***
Force Tags              References: JIRA-Ticket-ID
Resource                ${EXECDIR}${/}Resources${/}Common.robot

*** Variables ***
${MOBILENUMBER_START}   9543216000

*** Keywords ***
Generate A Rapid Pass
    [Arguments]         ${increment}
    ${new_msisdn}=      Evaluate                ${MOBILENUMBER_START} + ${increment}
    # Setup
    ${single_newRequestPass_details}=           RapidPassPH-Helpers.Generate Random Single newRequestPass Details       requester_type=INDIVIDUAL
    ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}=      RapidPassPH-API-Requests.Single newRequestPass          aporType=${single_newRequestPass_details}[aporType]
    ...                                                                                                                                             company=${single_newRequestPass_details}[company]
    ...                                                                                                                                             destCity=${single_newRequestPass_details}[destCity]
    ...                                                                                                                                             destName=${single_newRequestPass_details}[destName]
    ...                                                                                                                                             destProvince=${single_newRequestPass_details}[destProvince]
    ...                                                                                                                                             destStreet=${single_newRequestPass_details}[destStreet]
    ...                                                                                                                                             email=${single_newRequestPass_details}[email]
    ...                                                                                                                                             firstName=${single_newRequestPass_details}[firstName]
    ...                                                                                                                                             idType=${single_newRequestPass_details}[idType]
    ...                                                                                                                                             identifierNumber=${single_newRequestPass_details}[identifierNumber]
    ...                                                                                                                                             lastName=${single_newRequestPass_details}[lastName]
    ...                                                                                                                                             middleName=${single_newRequestPass_details}[middleName]
    ...                                                                                                                                             mobileNumber=0${new_msisdn}
    ...                                                                                                                                             originCity=${single_newRequestPass_details}[originCity]
    ...                                                                                                                                             originName=${single_newRequestPass_details}[originName]
    ...                                                                                                                                             originProvince=${single_newRequestPass_details}[originProvince]
    ...                                                                                                                                             originStreet=${single_newRequestPass_details}[originStreet]
    ...                                                                                                                                             passType=${single_newRequestPass_details}[passType]
    ...                                                                                                                                             plateNumber=${single_newRequestPass_details}[plateNumber]
    ...                                                                                                                                             remarks=${single_newRequestPass_details}[remarks]
    ...                                                                                                                                             suffix=${single_newRequestPass_details}[suffix]
    # Main Test
    ${response_object_getaccesspassdetails}=    RapidPassPH-API-Requests.getAccessPassDetails           referenceId=0${new_msisdn}
    Run Keyword And Continue On Failure         Should Be Equal As Strings      200         ${response_object_getaccesspassdetails.status_code}
    Log To Console  Done: 0${new_msisdn}
    [Return]        ${response_object_single_newrequestpass}    ${request_data_single_newrequestpass}

*** Tasks ***
Generate RapidPasses
    [Documentation]         Fails if HTTP status code is not 200.
    ...                     Fails if response body json key-value pair is not found.
    FOR     ${increment}    IN RANGE    0       101
            Run Keyword And Continue On Failure     Generate A Rapid Pass       ${increment}
    END
