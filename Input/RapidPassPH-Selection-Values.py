RAPIDPASSPH_SELECTION_VALUES_APORTYPE = [
	"AG",
	"BA",
	"BP",
	"CA",
	"DC",
	"DO",
	"ER",
	"FC",
	"FS",
	"GO",
	"GR",
	"HM",
	"HT",
	"IP",
	"LW",
	"ME",
	"MS",
	"MF",
	"MT",
	"PH",
	"PM",
	"PI",
	"SH",
	"SS",
	"TF",
	"UT",
	"VE"
]

# Access all keys via ```${query}=               Evaluate    __import__('jsonpath_rw_ext').match("$[*].*~", ${RAPIDPASSPH_SELECTION_VALUES_APORTYPE_WITH_DESCRIPTION})```
RAPIDPASSPH_SELECTION_VALUES_APORTYPE_WITH_DESCRIPTION = [
	{ "AG": "Agribusiness & Agricultural Workers" },
	{ "BA": "Banks" },
	{ "BP": "BPOs & Export-Oriented Business Personnel" },
	{ "CA": "Civil Aviation" },
	{ "DC": "Delivery personnel of cargoes" },
	{ "DO": "Distressed OFWs" },
	{ "ER": "Emergency Responders" },
	{ "FC": "Food Chain / Restaurants" },
	{ "FS": "Funeral Service" },
	{ "GO": "Government Agency" },
	{ "GR": "Grocery / Convenience Stores" },
	{ "HM": "Heads of Mission / Designated Foreign Mission Reps" },
	{ "HT": "Hotel Employees and Tenants" },
	{ "IP": "International Passengers and Driver" },
	{ "LW": "Logistics Warehouse" },
	{ "ME": "Media Personalities" },
	{ "MS": "Media Personalities" },
	{ "MF": "Manufacturing" },
	{ "MT": "Money Transfer Services" },
	{ "PH": "Pharmacies / Drug Stores" },
	{ "PM": "Public Market" },
	{ "PI": "Private Individual (1 per HH) availing the services of the mentioned establishments" },
	{ "SH": "Ship Captain & Crew" },
	{ "SS": "Security Services" },
	{ "TF": "Transportation Facilities" },
	{ "UT": "Utilities (Sanitation, Water, Energy and Telecommunications)" },
	{ "VE": "Veterinary" }
]

RAPIDPASSPH_SELECTION_VALUES_IDTYPE = [
	"PersonalID",
	"VehicleID"
]

RAPIDPASSPH_SELECTION_VALUES_PASSTYPE = [
	"INDIVIDUAL",
	"VEHICLE"
]

RAPIDPASSPH_SELECTION_VALUES_STATUS = [
	"PENDING",
	"APPROVED",
	"DECLINED",
	"SUSPENDED"
]

RAPIDPASSPH_WEB_SELECTION_VALUES_ID_TYPE = [
	"Company ID",
	"Diplomat ID",
	"Driver's License",
	"GSIS/UMID",
	"NBI Clearance",
	"OFW",
	"Pag-IBIG",
	"Passport",
	"PhilHealth",
	"Postal",
	"PRC",
	"PWD",
	"Senior Citizen",
	"SSS/UMID",
	"TIN",
	"Voter's ID"
]

RAPIDPASSPH_WEB_SELECTION_VALUES_VEHICLE_ID_TYPE = [
	"Plate Number",
	"Conduction Sticker"
]