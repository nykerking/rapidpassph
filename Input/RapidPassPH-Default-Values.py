RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_INDIVIDUAL = {
	"APORTYPE": "PI",
	"COMPANY": "Kumpanya+",
	"DESTCITY": "Parañaque City",
	"DESTNAME": "Medical Center Parañaque",
	"DESTPROVINCE": "Metro Manila",
	"DESTSTREET": "Dr. Aracadio Santos Ave.",
	"EMAIL": "nyker.test@gmail.com",
	"FIRSTNAME": "rekyn+Ñiña+",
	"IDTYPE": "PersonalID",
	"IDENTIFIERNUMBER": "N04-07-S06888",
	"LASTNAME": "gnik+Ñiñi+",
	"MIDDLENAME": "yok+Ñiñe+",
	"MOBILENUMBER": "09111156789",
	"ORIGINCITY": "Las Piñas City",
	"ORIGINNAME": "Las Piñas Medical Center",
	"ORIGINPROVINCE": "Metro Manila",
	"ORIGINSTREET": "1314 Marcos Alvarez Ave.",
	"PASSTYPE": "INDIVIDUAL",
	"REMARKS": "VIP",
	"SUFFIX": "Sr."
}

RAPIDPASSPH_DEFAULT_VALUES_SINGLE_NEWREQUESTPASS_PASSTYPE_VEHICLE = {
	"APORTYPE": "PI",
	"COMPANY": "Kumpanya+",
	"DESTCITY": "Parañaque City",
	"DESTNAME": "Medical Center Parañaque",
	"DESTPROVINCE": "Metro Manila",
	"DESTSTREET": "Dr. Aracadio Santos Ave.",
	"EMAIL": "nyker.test@gmail.com",
	"FIRSTNAME": "rekyn+Ñiña+",
	"IDTYPE": "PersonalID",
	"IDENTIFIERNUMBER": "N04-07-S06888",
	"LASTNAME": "gnik+Ñiñi+",
	"MIDDLENAME": "yok+Ñiñe+",
	"MOBILENUMBER": "09111156789",
	"NAME": "Rekyn Yok Gnik + ",
	"ORIGINCITY": "Las Piñas City",
	"ORIGINNAME": "Las Piñas Medical Center",
	"ORIGINPROVINCE": "Metro Manila",
	"ORIGINSTREET": "1314 Marcos Alvarez Ave.",
	"PASSTYPE": "VEHICLE",
	"PLATENUMBER": "AKA 1023",
	"REFNUM": "REKYN_USE_DEFAULT",
	"REMARKS": "VIP",
	"SUFFIX": "Sr."
}

RAPIDPASSPH_DEFAULT_VALUES_UPDATEACCESSPASS = {
	# "APORTYPE": "FC",
	# "COMPANY": "Updated Kumpanya+",
	# "CONTROLCODE": "REKYN_USE_DEFAULT",
	# "DESTCITY": "Manila City",
	# "DESTNAME": "San Lazaro Hospital",
	# "DESTPROVINCE": "Metro Manila",
	# "DESTSTREET": "Quiricada St.",
	# "IDTYPE": "VehicleID",
	# "IDENTIFIERNUMBER": "8",
	# "NAME": "Updated Rekyn Yok Gnik + ",
	# "ORIGINCITY": "Las Piñas City",
	# "ORIGINNAME": "Las Piñas Medical Center",
	# "ORIGINPROVINCE": "Metro Manila",
	# "ORIGINSTREET": "1314 Marcos Alvarez Ave.",
	# "PASSTYPE": "Vehicle",
	# "REMARKS": "Lucky Me! Spicy Bulalo",
	"REFERENCEID": "09222256789",
	"STATUS": "DECLINED"#,
	# "VALIDFROM": "REKYN_USE_DEFAULT",
	# "VALIDUNTIL": "REKYN_USE_DEFAULT"
}

RAPIDPASSPH_DEFAULT_VALUES_GETACCESSPASSDETAILS = {
	"MOBILENUMBER": "09333356789"
}

RAPIDPASSPH_DEFAULT_VALUES_GETACCESSPASSES = {
	"MOBILENUMBER": "09444456789"
}

RAPIDPASSPH_DEFAULT_VALUES_VALID_UNTIL = {
	"YEAR": 2020,
	"MONTH": 4,
	"DAY": 30
}
