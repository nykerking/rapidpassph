*** Variables ***
${RAPIDPASSPH_API_DOMAIN}                   rapidpass-api.azurewebsites.net
${RAPIDPASSPH_API_PROTOCOL}                 https
${RAPIDPASSPH_API_URL_BASE}                 ${RAPIDPASSPH_API_PROTOCOL}://${RAPIDPASSPH_API_DOMAIN}
${RAPIDPASSPH_API_VERSION}                  v1
${RAPIDPASSPH_API_URL_WHOLE}                ${RAPIDPASSPH_API_URL_BASE}/api/${RAPIDPASSPH_API_VERSION}

# Othere URLs
# Prod: https://rapidpass-api.azurewebsites.net
# Staging: https://rapidpass-api-stage.azurewebsites.net
