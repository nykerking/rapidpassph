RAPIDPASSPH_API_RULES = {
	"GETACCESSPASSDETAILS": {
		"EXPECTED_FIELDS": [
			"aporType",
			"company",
			"controlCode",
			"destCity",
			"destName",
			"destProvince",
			"destStreet",
			"identifierNumber",
			"idType",
			"name",
			"passType",
			"plateNumber",
			"referenceId",
			"remarks",
			"status",
			"validFrom",
			"validUntil"
		]
	},
	"RECREATABLE_RAPIDPASS_STATUS": [
		"DECLINED",
		"SUSPENDED"
	],
	"SINGLE_NEWREQUESTPASS": {
		"REQUIRED_FIELDS": [
			"aporType",
			"company",
			"destCity",
			"destName",
			"destProvince",
			"destStreet",
			"email",
			"firstName",
			"identifierNumber",
			"idType",
			"lastName",
			"mobileNumber",
			"originCity",
			"originName",
			"originProvince",
			"originStreet",
			"passType"
		]
	},
	"UPDATEACCESSPASS": {
		"REQUIRED_FIELDS": [
			"status"
		]
	}
}
