*** Variables ***
${RAPIDPASSPH_WEB_DOMAIN_PROD}              rapidpass-registration.azurewebsites.net/
${RAPIDPASSPH_WEB_DOMAIN}                   rapidpass-registration-staging.azurewebsites.net
${RAPIDPASSPH_WEB_PROTOCOL}                 https
${RAPIDPASSPH_WEB_URL}                      ${RAPIDPASSPH_WEB_PROTOCOL}://${RAPIDPASSPH_WEB_DOMAIN}
