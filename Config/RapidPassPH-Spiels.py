# -*- coding: utf-8 -*-

RAPIDPASSPH_API_SPIELS = {
	"ERR": {
		"INVALID_STATUS_TRANSITION": "Request Status not yet supported!",
		"INVALID_STATUS_NAME": "Unknown status code.",
		"NULL_FIELD": "must not be null",
		"REQUIRED_FIELD": "must not be empty"
	}
}
